<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "Ligar ou deligar a busca e sincronização das agendas.">
<!ENTITY colorAccountsTooltip "Colorir os resultados da pesquisa.">
<!ENTITY readonlyAccountsTooltip "Colocar agendas em modo só de leitura ou em modo de leitura e escrita.">
<!ENTITY searchRemoteLabel "Este catálogo de endereços mostra contatos somente após uma pesquisa">

<!ENTITY cardbookAccountMenuLabel "Agenda">

<!ENTITY cardbookAccountMenuAddServerLabel "Nova agenda">
<!ENTITY cardbookAccountMenuEditServerLabel "Editar agenda">
<!ENTITY cardbookAccountMenuCloseServerLabel "Apagar agenda">
<!ENTITY cardbookAccountMenuSyncLabel "Sincronizar agenda">
<!ENTITY cardbookAccountMenuSyncsLabel "Sincronizar todas as agendas">

<!ENTITY cardbookContactsMenuLabel "Contatos">

<!ENTITY cardbookToolsMenuLabel "Ferramentas">
<!ENTITY cardbookToolsMenuBirthdayListLabel "Ver lista de aniversários">
<!ENTITY cardbookToolsMenuSyncLightningLabel "Adicionar aniversário ao calendário">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "Procurar duplicados em todas as agendas">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "Procurar duplicados na agenda atual">
<!ENTITY cardbookToolsMenuPrefsLabel "Preferências do CardBook">

<!ENTITY cardbookToolbarLabel "Barra de ferramentas do CardBook">
<!ENTITY cardbookToolbarAccesskey "C">
<!ENTITY cardbookABPaneToolbarLabel "Barra de ferramentas da agenda do CardBook">
<!ENTITY cardbookABPaneToolbarAccesskey "d">
<!ENTITY CustomizeCardBookToolbarLabel "Personalizar…">
<!ENTITY CustomizeCardBookToolbarAccesskey "P">
<!ENTITY cardbookToolbarAddServerButtonLabel "Nova agenda">
<!ENTITY cardbookToolbarAddServerButtonTooltip "Adicionar uma agenda local ou remota">
<!ENTITY cardbookToolbarSyncButtonLabel "Sincronizar">
<!ENTITY cardbookToolbarSyncButtonTooltip "Sincronizar todas as agendas remotas">
<!ENTITY cardbookToolbarWriteButtonLabel "Escrever">
<!ENTITY cardbookToolbarWriteButtonTooltip "Criar uma nova mensagem">
<!ENTITY cardbookToolbarChatButtonLabel "Conectar com">
<!ENTITY cardbookToolbarChatButtonTooltip "Enviar mensagem instantânea ou de chat">
<!ENTITY cardbookToolbarConfigurationButtonLabel "Preferências">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "Preferências do CardBook">
<!ENTITY cardbookToolbarAddContactButtonLabel "Novo contato">
<!ENTITY cardbookToolbarAddContactButtonTooltip "Criar novo contato na agenda">
<!ENTITY cardbookToolbarAddListButtonLabel "Nova lista">
<!ENTITY cardbookToolbarAddListButtonTooltip "Criar nova lista">
<!ENTITY cardbookToolbarEditButtonLabel "Editar">
<!ENTITY cardbookToolbarEditButtonTooltip "Permitir alteração de contato">
<!ENTITY cardbookToolbarRemoveButtonLabel "Apagar">
<!ENTITY cardbookToolbarRemoveButtonTooltip "Permitir apagar contato">
<!ENTITY cardbookToolbarPrintButtonLabel "Pré-visualização">
<!ENTITY cardbookToolbarPrintButtonTooltip "Pré-visualização da seleção">
<!ENTITY cardbookToolbarBackButtonLabel "Desfazer">
<!ENTITY cardbookToolbarBackButtonTooltip "Voltar uma ação">
<!ENTITY cardbookToolbarForwardButtonLabel "Refazer">
<!ENTITY cardbookToolbarForwardButtonTooltip "Adiantar uma ação">
<!ENTITY cardbookToolbarAppMenuButtonLabel "Menu CardBook">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "Menu CardBook">
<!ENTITY cardbookToolbarSearchBoxLabel "Busca por contatos">
<!ENTITY cardbookToolbarSearchBoxTooltip "Busca por contatos">
<!ENTITY cardbookToolbarComplexSearchLabel "Busca avançada">
<!ENTITY cardbookToolbarComplexSearchTooltip "Criar um novo diretório de buscas salvas">
<!ENTITY cardbookToolbarThMenuButtonLabel "Menu">
<!ENTITY cardbookToolbarThMenuButtonTooltip "Mostra o menu &brandShortName;">
<!ENTITY cardbookToolbarABMenuLabel "Ver livro de endereços">
<!ENTITY cardbookToolbarABMenuTooltip "Filtro como agendas são exibidas">

<!ENTITY generalTabLabel "Geral">
<!ENTITY mailPopularityTabLabel "Popularidade do email">
<!ENTITY technicalTabLabel "Técnico">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "Categorias">
<!ENTITY noteTabLabel "Notas">

<!ENTITY miscGroupboxLabel "Diversos">
<!ENTITY othersGroupboxLabel "Outros">
<!ENTITY techGroupboxLabel "Técnicos">
<!ENTITY labelGroupboxLabel "Etiquetas">
<!ENTITY class1Label "Classes">
<!ENTITY geoLabel "Geo">
<!ENTITY mailerLabel "Mailer">
<!ENTITY agentLabel "Agente">
<!ENTITY keyLabel "Chave">
<!ENTITY photolocalURILabel "Imagem local">
<!ENTITY logolocalURILabel "Logotipo local">
<!ENTITY soundlocalURILabel "Som local">
<!ENTITY photoURILabel "Fotografia">
<!ENTITY logoURILabel "Logo">
<!ENTITY soundURILabel "Som">
<!ENTITY prodidLabel "Prodid">
<!ENTITY sortstringLabel "Texto para ordenação">
<!ENTITY uidLabel "Id do contato">
<!ENTITY versionLabel "Versão">
<!ENTITY tzLabel "Fuso horário">

<!ENTITY dirPrefIdLabel "Id da agenda">
<!ENTITY cardurlLabel "URL do cartão">
<!ENTITY cacheuriLabel "URI do cache">
<!ENTITY revLabel "Última atualização">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "Mostrar no mapa">
<!ENTITY toEmailEmailTreeLabel "Escrever nova mensagem">
<!ENTITY ccEmailEmailTreeLabel "Escrever nova mensagem (cc)">
<!ENTITY bccemailemailTreeLabel "Escrever nova mensagem (bcc)">
<!ENTITY findemailemailTreeLabel "Procurar emails com este endereço de email">
<!ENTITY findeventemailTreeLabel "Procurar eventos no calendário relacionados com este email">
<!ENTITY openURLTreeLabel "Abrir URL">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "Escrever nova mensagem">
<!ENTITY toEmailCardFromCardsLabel "Escrever nova mensagem">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "Escrever nova mensagem (cc)">
<!ENTITY ccEmailCardFromCardsLabel "Escrever nova mensagem (cc)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "Escrever nova mensagem (bcc)">
<!ENTITY bccEmailCardFromCardsLabel "Escrever nova mensagem (bcc)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "Enviar por email">
<!ENTITY shareCardByEmailFromCardsLabel "Enviar por email">
<!ENTITY categoryLabel "Categoria">
<!ENTITY findEmailsFromCardsLabel "Procurar emails com este endereço de email">
<!ENTITY findEventsFromCardsLabel "Procurar eventos no calendário relacionados com este contato">
<!ENTITY localizeCardFromCardsLabel "Mostrar no mapa">
<!ENTITY openURLCardFromCardsLabel "Abrir URL">
<!ENTITY cutCardFromAccountsOrCatsLabel "Cortar">
<!ENTITY cutCardFromCardsLabel "Cortar">
<!ENTITY copyCardFromAccountsOrCatsLabel "Copiar">
<!ENTITY copyCardFromCardsLabel "Copiar">
<!ENTITY pasteCardFromAccountsOrCatsLabel "Colar">
<!ENTITY pasteCardFromCardsLabel "Colar">
<!ENTITY pasteEntryLabel "Colar entrada">
<!ENTITY exportCardToFileLabel "Exportar para arquivo">
<!ENTITY exportCardToDirLabel "Exportar para diretório">
<!ENTITY importCardFromFileLabel "Importar cartões de um arquivo">
<!ENTITY importCardFromDirLabel "Importar cartões de um diretório">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "Procurar duplicados na agenda atual">
<!ENTITY generateFnFromAccountsOrCatsLabel "Gerar nomes de apresentação">
<!ENTITY mergeCardsFromCardsLabel "Juntar cartões">
<!ENTITY duplicateCardFromCardsLabel "Contato duplicado">
<!ENTITY convertListToCategoryFromCardsLabel "Converter lista para categoria">
<!ENTITY editAccountFromAccountsOrCatsLabel "Editar agenda">
<!ENTITY syncAccountFromAccountsOrCatsLabel "Sincronizar agenda">
<!ENTITY removeAccountFromAccountsOrCatsLabel "Remover agenda">

<!ENTITY IMPPMenuLabel "Conectar com">
