<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "주소록 검색 및 동기화 사용 또는 사용 안 함.">
<!ENTITY colorAccountsTooltip "주소록 검색 결과에 색상 적용 (검색이 여러 주소록에 걸쳐있을 때 사용).">
<!ENTITY readonlyAccountsTooltip "주소록은 읽기 전용 또는 읽기-쓰기 모드로 설정하십시오.">
<!ENTITY searchRemoteLabel "이 주소록은 검색된 연락처만 보여줍니다">

<!ENTITY cardbookAccountMenuLabel "주소록 ">

<!ENTITY cardbookAccountMenuAddServerLabel "새 주소록">
<!ENTITY cardbookAccountMenuEditServerLabel "주소록 편집">
<!ENTITY cardbookAccountMenuCloseServerLabel "주소록 삭제">
<!ENTITY cardbookAccountMenuSyncLabel "주소록 동기화">
<!ENTITY cardbookAccountMenuSyncsLabel "모든 주소록 동기화">

<!ENTITY cardbookContactsMenuLabel "연락처">

<!ENTITY cardbookToolsMenuLabel "도구">
<!ENTITY cardbookToolsMenuBirthdayListLabel "기념일 목록보기">
<!ENTITY cardbookToolsMenuSyncLightningLabel "캘린더에 기념일 추가">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "모든 주소록에서 중복된 연락처 찾기">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "현재 주소록에서 중복된 연락처 찾기">
<!ENTITY cardbookToolsMenuPrefsLabel "연락처 환경 설정">

<!ENTITY cardbookToolbarLabel "연락처 도구 모음">
<!ENTITY cardbookToolbarAccesskey "a">
<!ENTITY cardbookABPaneToolbarLabel "연락처 주소록 창 도구 모음">
<!ENTITY cardbookABPaneToolbarAccesskey "d">
<!ENTITY CustomizeCardBookToolbarLabel "사용자 정의…">
<!ENTITY CustomizeCardBookToolbarAccesskey "C">
<!ENTITY cardbookToolbarAddServerButtonLabel "새 주소록">
<!ENTITY cardbookToolbarAddServerButtonTooltip "원격 또는 로컬 주소록 추가">
<!ENTITY cardbookToolbarSyncButtonLabel "동기화">
<!ENTITY cardbookToolbarSyncButtonTooltip "모든 원격 주소록 동기화">
<!ENTITY cardbookToolbarWriteButtonLabel "쓰기">
<!ENTITY cardbookToolbarWriteButtonTooltip "새 메시지 만들기">
<!ENTITY cardbookToolbarChatButtonLabel "~에 연결하다">
<!ENTITY cardbookToolbarChatButtonTooltip "인스턴트 메시지 또는 채팅 보내기">
<!ENTITY cardbookToolbarConfigurationButtonLabel "환경 설정">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "CardBook 환경 설정">
<!ENTITY cardbookToolbarAddContactButtonLabel "새 연락처">
<!ENTITY cardbookToolbarAddContactButtonTooltip "새 주소록 연락처 만들기">
<!ENTITY cardbookToolbarAddListButtonLabel "새 목록">
<!ENTITY cardbookToolbarAddListButtonTooltip "새 메일링 목록">
<!ENTITY cardbookToolbarEditButtonLabel "수정">
<!ENTITY cardbookToolbarEditButtonTooltip "선택한 연락처 수정">
<!ENTITY cardbookToolbarRemoveButtonLabel "삭제">
<!ENTITY cardbookToolbarRemoveButtonTooltip "선택한 연락처 삭제">
<!ENTITY cardbookToolbarPrintButtonLabel "인쇄 미리보기">
<!ENTITY cardbookToolbarPrintButtonTooltip "선택한 항목의 인쇄 미리보기">
<!ENTITY cardbookToolbarBackButtonLabel "실행 취소">
<!ENTITY cardbookToolbarBackButtonTooltip "한 단계 이전으로">
<!ENTITY cardbookToolbarForwardButtonLabel "다시 실행">
<!ENTITY cardbookToolbarForwardButtonTooltip "한 단계 다시 실행">
<!ENTITY cardbookToolbarAppMenuButtonLabel "CardBook 메뉴">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "CardBook  메뉴">
<!ENTITY cardbookToolbarSearchBoxLabel "연락처 검색">
<!ENTITY cardbookToolbarSearchBoxTooltip "연락처 검색">
<!ENTITY cardbookToolbarComplexSearchLabel "저장된 검색">
<!ENTITY cardbookToolbarComplexSearchTooltip "새 저장된 검색 폴더 만들기">
<!ENTITY cardbookToolbarThMenuButtonLabel "응용 메뉴">
<!ENTITY cardbookToolbarThMenuButtonTooltip "&brandShortName; 메뉴 표시">
<!ENTITY cardbookToolbarABMenuLabel "주소록 보기">
<!ENTITY cardbookToolbarABMenuTooltip "주소록 표시 필터">

<!ENTITY generalTabLabel "일반">
<!ENTITY mailPopularityTabLabel "메일 인기도">
<!ENTITY technicalTabLabel "세부 정보">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "카테고리">
<!ENTITY noteTabLabel "메모">

<!ENTITY miscGroupboxLabel "기타">
<!ENTITY othersGroupboxLabel "기타">
<!ENTITY techGroupboxLabel "세부 정보">
<!ENTITY labelGroupboxLabel "레이블">
<!ENTITY class1Label "분류">
<!ENTITY geoLabel "지역">
<!ENTITY mailerLabel "우편 발신인">
<!ENTITY agentLabel "에이전트">
<!ENTITY keyLabel "키">
<!ENTITY photolocalURILabel "로컬 사진">
<!ENTITY logolocalURILabel "로컬 로고">
<!ENTITY soundlocalURILabel "로컬 소리">
<!ENTITY photoURILabel "사진">
<!ENTITY logoURILabel "로고">
<!ENTITY soundURILabel "소리">
<!ENTITY prodidLabel "Prodid">
<!ENTITY sortstringLabel "문자열 정렬">
<!ENTITY uidLabel "연락처 id">
<!ENTITY versionLabel "버전">
<!ENTITY tzLabel "시간대">

<!ENTITY dirPrefIdLabel "주소록 id">
<!ENTITY cardurlLabel "카드 URL">
<!ENTITY cacheuriLabel "캐시 URI">
<!ENTITY revLabel "마지막 업데이트">
<!ENTITY etagLabel "전자 태그">

<!ENTITY localizeadrTreeLabel "지도에 표시">
<!ENTITY toEmailEmailTreeLabel "새 메시지 쓰기">
<!ENTITY ccEmailEmailTreeLabel "새 메시지 쓰기(참조)">
<!ENTITY bccemailemailTreeLabel "새 메시지 쓰기(숨은 참조)">
<!ENTITY findemailemailTreeLabel "이 이메일 주소와 관련된 이메일 찾기">
<!ENTITY findeventemailTreeLabel "이 이메일 주소와 관련된 캘린더 일정 찾기">
<!ENTITY openURLTreeLabel "URL 열기">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "새 메시지 쓰기">
<!ENTITY toEmailCardFromCardsLabel "새 메시지 쓰기">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "새 메시지 쓰기(참조)">
<!ENTITY ccEmailCardFromCardsLabel "새 메시지 쓰기(참조)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "새 메시지 쓰기(숨은 참조)">
<!ENTITY bccEmailCardFromCardsLabel "새 메시지 쓰기(숨은 참조)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "이메일로 공유">
<!ENTITY shareCardByEmailFromCardsLabel "이메일로 공유">
<!ENTITY categoryLabel "카테고리">
<!ENTITY findEmailsFromCardsLabel "이 연락처와 관련된 이메일을 찾기">
<!ENTITY findEventsFromCardsLabel "이 연락처와 관련된 캘린더 이벤트 찾기">
<!ENTITY localizeCardFromCardsLabel "지도에 표시">
<!ENTITY openURLCardFromCardsLabel "URL 열기">
<!ENTITY cutCardFromAccountsOrCatsLabel "잘라내기">
<!ENTITY cutCardFromCardsLabel "잘라내기">
<!ENTITY copyCardFromAccountsOrCatsLabel "복사">
<!ENTITY copyCardFromCardsLabel "복사">
<!ENTITY pasteCardFromAccountsOrCatsLabel "붙여넣기">
<!ENTITY pasteCardFromCardsLabel "붙여넣기">
<!ENTITY pasteEntryLabel "항목 붙여넣기">
<!ENTITY exportCardToFileLabel "파일로 내보내기">
<!ENTITY exportCardToDirLabel "디렉토리로 내보내기">
<!ENTITY importCardFromFileLabel "파일에서 연락처 가져 오기">
<!ENTITY importCardFromDirLabel "디렉토리에서 연락처 가져 오기">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "현재 주소록에서 중복된 연락처 찾기">
<!ENTITY generateFnFromAccountsOrCatsLabel "표시 이름 생성">
<!ENTITY mergeCardsFromCardsLabel "연락처 병합">
<!ENTITY duplicateCardFromCardsLabel "연락처 복제">
<!ENTITY convertListToCategoryFromCardsLabel "목록을 카테고리로 변환">
<!ENTITY editAccountFromAccountsOrCatsLabel "주소록 편집">
<!ENTITY syncAccountFromAccountsOrCatsLabel "주소록 동기화">
<!ENTITY removeAccountFromAccountsOrCatsLabel "주소록 삭제">

<!ENTITY IMPPMenuLabel "~에 연결하다">
