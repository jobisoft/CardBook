<clientConfig version="1.1">
	<carddavProvider id="CardDAVEngine">
		<carddavURL>https://contacts.zoho.com/.well-known/carddav</carddavURL>
		<vCardVersion>3.0</vCardVersion>
	</carddavProvider>
</clientConfig>